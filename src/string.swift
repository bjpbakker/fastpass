// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

extension String {
    func lastIndexBeforeEnd(where p: (Character) -> Bool) -> String.Index? {
        if let index = self.lastIndex(where: p) {
            if index.encodedOffset == (self.endIndex.encodedOffset - 1) {
                return String(self[..<index]).lastIndex(where: p)
            } else {
                return index
            }
        }
        return nil
    }
}

extension StringProtocol {
    func occur(_ s: String) -> [Self.Index] {
        guard s.count > 0 else {
            fatalError("TypeError: non-empty string required")
        }

        var occurs: [Self.Index] = []
        var mut = self[startIndex..<endIndex]
        while !mut.isEmpty {
            if mut.hasPrefix(s) {
                occurs.append(mut.startIndex)
                mut = mut.dropFirst(s.count)
            } else {
                mut = mut.dropFirst(1)
            }
        }
        return occurs
    }

    func words(boundaries: [Character] = [" "]) -> [String] {
        if self.isEmpty {
            return []
        }

        if let idx = boundaries.compactMap({ firstIndex(of: $0) }).min() {
            let rec = self[idx..<endIndex].dropFirst(1).words
            let word = idx > startIndex ? [String(self[startIndex..<idx])] : []
            return word + rec(boundaries)
        }

        return [String(self)]
    }

    func lines(omitEmpty: Bool = true) -> [SubSequence] {
        return self.split(separator: "\n", omittingEmptySubsequences: omitEmpty)
    }
}

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

enum Result<E, T> {
    case success(T), failure(E)

    func map<R>(_ f: (T) -> R) -> Result<E, R> {
        return bind({.success(f($0))})
    }

    func bind<R>(_ f: (T) -> Result<E, R>) -> Result<E, R> {
        switch self {
        case .success(let x):
             return f(x)
        case .failure(let err):
             return .failure(err)
        }
    }
}

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Cocoa

func font(size: CGFloat) -> NSFont {
    let fonts = ["Fira Code", "Hack", "Menlo"]
    let result = fonts.reduce(
      nil,
      { acc, candidate in acc ?? NSFont(name: candidate, size: size) }
    )
    return result ?? NSFont.systemFont(ofSize: size)
}

func Label(text: String, fontSize: CGFloat = 18) -> NSTextField {
    let label = NSTextField.init(string: text)
    label.isEnabled = false
    label.isSelectable = false
    label.isBordered = false
    label.backgroundColor = .clear
    label.font = font(size: fontSize)
    return label
}

class MainWindow: NSWindow {
    static let width: CGFloat = 640

    let searchField: NSTextField = {
        let field = NSTextField.init(frame: NSMakeRect(0,0,width,60))
        field.isBordered = false
        field.isEnabled = true
        field.isSelectable = false
        field.focusRingType = .none
        field.font = font(size: 32)
        field.textColor = NSColor(red:0.129, green:0.129, blue:0.129, alpha:1.000)
        field.backgroundColor = NSColor(red:0.925, green:0.937, blue:0.945, alpha:1.000)

        return field
    }()

    let passwordData: PasswordTableData = PasswordTableData()
    let passwordTable: NSTableView = {
        let table = NSTableView.init(frame: NSMakeRect(0,0,width,0))
        table.rowSizeStyle = .large
        table.backgroundColor = .clear
        table.headerView = nil
        table.columnAutoresizingStyle = .noColumnAutoresizing

        let column = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(rawValue: "column"))
        column.width = width
        table.addTableColumn(column)

        return table
    }()

    init() {
        let rect = NSMakeRect(0, 0, MainWindow.width, 100)

        super.init(
          contentRect: rect,
          styleMask: [.titled, .fullSizeContentView],
          backing: .buffered,
          defer: true
        )
        titleVisibility = .hidden
        titlebarAppearsTransparent = true
        hasShadow = false

        passwordTable.delegate = passwordData
        passwordTable.dataSource = passwordData

        let superview = NSView.init(frame: frame)
        superview.addSubview(searchField)
        superview.addSubview(passwordTable)

        self.contentView = superview
    }

    func render(_ model: Model) {
        searchField.stringValue = " > " + model.query + "_"

        passwordData.reload(passwords: model.matches, selected: model.selected)
        passwordTable.isHidden = passwordData.isEmpty
        passwordTable.reloadData()

        let tableHeight = [passwordTable.frame.height, 340].min()!
        let height = searchField.frame.height + tableHeight
        searchField.frame = NSMakeRect(0, height - searchField.frame.height, frame.width, searchField.frame.height)
        passwordTable.frame = NSMakeRect(0,0,frame.width, tableHeight)
        passwordTable.bounds = NSMakeRect(0,0,frame.width, tableHeight)

        setFrame(
          NSRect(
            origin: CGPoint(x: frame.origin.x, y: frame.origin.y + frame.height - height),
            size: CGSize(width: frame.width, height: height)
          ),
          display: true,
          animate: false
        )
    }
}

class PasswordTableData: NSObject, NSTableViewDelegate, NSTableViewDataSource {
    var passwords: [Password] = []
    var selected: Int = 0

    func reload(passwords: [Password], selected: Int) {
        self.passwords = passwords
        self.selected = selected
    }

    var isEmpty: Bool {
        get { return passwords.isEmpty }
    }

    func numberOfRows(in tableView: NSTableView) -> Int {
        return passwords.count
    }

    func tableView(
      _ tableView: NSTableView,
      viewFor tableColumn: NSTableColumn?,
      row: Int
    ) -> NSView? {
        let password = passwords[row]

        let view = Label(text: password.show())
        if row == selected {
            view.backgroundColor = NSColor(red:0.000, green:0.592, blue:0.655, alpha:0.2)
        }
        return view
    }

    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return 26
    }

    func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        return false
    }
}

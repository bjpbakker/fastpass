// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Cocoa

class AppController: NSObject, NSApplicationDelegate, NSUserNotificationCenterDelegate {
    weak var notificationCenter = NSUserNotificationCenter.default

    let mainWindow: MainWindow = MainWindow()

    var state: MutRef<Model> = MutRef(Model.empty())

    let statusItem: NSStatusItem = {
        let item = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
        item.button?.image = NSImage.init(named: "FastPass")
        item.button?.image?.size = NSSize(width: 32, height: 32)
        item.button?.toolTip = "FastPass"
        item.button?.isHighlighted = false
        item.button?.action = #selector(handleStatusBarClick(_:))

        return item
    }()

    func applicationDidFinishLaunching(_ notification: Notification) {
        notificationCenter!.delegate = self

        state.read(render)

        NSEvent.addLocalMonitorForEvents(matching: .keyDown, handler: keymap)
        if AXIsProcessTrusted() {
            NSEvent.addGlobalMonitorForEvents(matching: .keyDown, handler: globalKeymap)
        }
    }

    func applicationWillBecomeActive(_ notification: Notification) {
        self.activate()
    }

    func applicationShouldTerminateAfterLastWindowClosed(_ app: NSApplication) -> Bool {
        return false
    }

    func userNotificationCenter(
      _ center: NSUserNotificationCenter,
      shouldPresent notification: NSUserNotification) -> Bool {
        return true
    }

    func render(_ model: Model) {
        DispatchQueue.main.async {
            self.mainWindow.render(model)
        }
    }

    @objc func activate() {
        DispatchQueue.global(qos: .userInitiated).async {
            let result = listPasswords()
            switch result {
            case .success(let passwords):
                self.state.swap { $0.update(passwords: passwords) }
            case .failure(let err):
                DispatchQueue.main.sync {
                    self.showNotification(identifier: "error", title: "Failed to load password store", text: err)
                }
            }
        }

        NSApplication.shared.activate(ignoringOtherApps: true)
        mainWindow.makeKeyAndOrderFront(nil)
        mainWindow.center()
    }

    func deactivate() {
        NSApplication.shared.hide(nil)
    }

    @objc func handleStatusBarClick(_ sender: Any?) {
        let app = NSApplication.shared
        if (app.isActive && mainWindow.isVisible) {
            self.deactivate()
        } else {
            self.activate()
        }
    }

    func showNotification(identifier: String, title: String, text: String) {
        let it = NSUserNotification()
        it.identifier = identifier
        it.title = title
        it.informativeText = text
        it.soundName = nil
        notificationCenter!.deliver(it)

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            self.notificationCenter!.removeDeliveredNotification(it)
        }
    }

    func keymap(event: NSEvent) -> NSEvent? {
        switch event.characters {
        case "q" where event.modifierFlags.intersection(.command) == .command:
            NSApplication.shared.terminate(self)
            return nil

        case "\u{1b}": // ^[
             fallthrough
        case "\u{07}": // ^G
            self.state.swap { $0.reset() }
            self.deactivate()
            return nil

        case "\u{15}": // ^U
            self.state.swap { $0.update(query: "") }
            return nil

        case "\u{17}": // ^W
            self.state.swap { m in
                if let index = m.query.lastIndexBeforeEnd(where: " /@.-_".contains) {
                    return m.update(query: String(m.query[...index]))
                } else {
                    return m.update(query: "")
                }
            }
            return nil

        case "\u{0d}": // CR
            self.state.swap { m in
                if m.selected >= m.matches.count {
                    return m.reset()
                }
                let password = m.matches[m.selected]
                DispatchQueue.global(qos: .userInitiated).async {
                    let copied = copyPassword(password)
                    switch copied {
                    case .success(let notice):
                        DispatchQueue.main.sync {
                            self.showNotification(identifier: password.show(), title: password.show(), text: notice)
                        }
                    case .failure(let err):
                        DispatchQueue.main.sync {
                            self.showNotification(identifier: "error", title: "Failed to copy \(password.show())", text: err)
                        }
                    }
                }
                return m.reset()
            }
            self.deactivate()
            return nil

        case "\u{7f}": // DEL
            self.state.swap { m in m.update(query: String(m.query.dropLast())) }
            return nil

        case "\u{f700}" where event.modifierFlags.intersection(.numericPad) == .numericPad:
            fallthrough
        case "\u{10}": // ^P
            self.state.swap { m in m.update(selected: m.selected - 1) }
            return nil

        case "\u{f701}" where event.modifierFlags.intersection(.numericPad) == .numericPad:
            fallthrough
        case "\u{0e}": // ^N
            self.state.swap { m in m.update(selected: m.selected + 1) }
            return nil

        case _ where event.modifierFlags.intersection([.control, .option, .command, .numericPad, .help, .function]) == []:
            self.state.swap { m in m.update(query: m.query + (event.characters ?? "")) }
            return nil

        case _:
            return event
        }
    }

    func globalKeymap(event: NSEvent) {
        if event.characters == "\u{10}" && event.modifierFlags.intersection([.control, .command]) == [.control, .command] { // s-^P
            self.activate()
        }
    }
}

func main() {
    let _ = NSApplication.shared

    let controller = AppController()
    NSApp.delegate = controller

    NSApp.run()
}

main()

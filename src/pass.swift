// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation

typealias FilePath = String

func readProcess(_ cmd: FilePath, _ args: [String], _ environment: [String: String]? = nil) -> Result<String, String> {
    let stdout = Pipe()

    let p = Process()
    p.launchPath = cmd
    p.arguments = args
    if let env = environment {
        p.environment = env
    }
    p.standardOutput = stdout

    do {
        try p.run()
    } catch {
        return .failure(error.localizedDescription)
    }

    p.waitUntilExit()
    if p.terminationStatus != 0 {
        return .failure("Process '\(cmd)' failed with exit code \(p.terminationStatus)")
    }

    let data = stdout.fileHandleForReading.readDataToEndOfFile()
    if let output = String.init(data: data, encoding: .utf8) {
        return .success(output)
    }
    return .failure("Failed to capture output of process '\(cmd)'")
}

func listPasswords() -> Result<String, [Password]> {
    return getenv().bind(
      { env in
          readProcess("/usr/bin/env", ["pass", "ls"], env).map(
            { output in
                let (passwords, _): ([Password], [String]) = output.lines()
                  .dropFirst()
                  .reduce(
                    ([], []),
                    { (acc, x) in
                        let (all, path) = acc
                        let indent = x.occur("|   ").count
                        let path_ = Array(path[0..<indent])
                        let name = String(x.drop(while: "|-` ".contains))

                        // Folders are shown with ansi color codes
                        if name.hasPrefix("\u{1b}[01;") {
                            return (all, path_ + [String(name.dropFirst(8).dropLast(5))])
                        }

                        return (all + [Password(name: name, path: path_)], path_)
                    }
                  )
                return passwords
            }
          )
      }
    )
}

func copyPassword(_ password: Password) -> Result<String, String> {
    return getenv().bind(
      { env in readProcess("/usr/bin/env", ["pass", "show", "--clip", password.show()], env) }
    )
}

func getenv() -> Result<String, [String: String]> {
    return execPathFromShell().map(
      { path in
          ["PATH": path,
           "HOME": NSHomeDirectory(),
           "TERM": "xterm-256color",
           "LANG": "en_US"]
      }
    )
}

func getShell() -> String {
    if let ptr = getenv("SHELL") {
        return String(cString: ptr)
    } else {
        return "/bin/sh"
    }
}

func execPathFromShell() -> Result<String, String> {
    return readProcess(getShell(), ["-l", "-c", "printenv"]).bind(
      { output in
          if let path = output.lines()
               .map({$0.split(separator: "=", maxSplits: 1)})
               .first(where: {$0[0] == "PATH"}) {
              return .success(String(path[1]))
          }
          return .failure("PATH environment variable is unset")
      }
    )
}

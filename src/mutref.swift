// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Cocoa

struct MutRef<T> {
    typealias RefReader = (T) -> Void

    private let lock: UnsafeMutablePointer<pthread_mutex_t>

    private var ref: T
    private var readers: [RefReader]

    init(_ ref: T) {
        self.lock = UnsafeMutablePointer.allocate(capacity: MemoryLayout<pthread_mutex_t>.size)
        pthread_mutex_init(self.lock, nil)

        self.ref = ref
        self.readers = []
    }

    mutating func read(_ f: @escaping RefReader) {
        pthread_mutex_lock(self.lock)
        readers.append(f)
        let take = self.ref
        pthread_mutex_unlock(self.lock)
        f(take)
    }

    @discardableResult
    mutating func swap(_ f: (T) -> T) -> T {
        pthread_mutex_lock(self.lock)
        let take = self.ref
        let value = f(take)
        self.ref = value
        pthread_mutex_unlock(self.lock)
        for r in readers {
            r(value)
        }
        return take
    }
}

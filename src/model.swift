// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

struct Model {
    let query: String
    let passwords: [Password]
    let matches: [Password]
    let selected: Int

    static func empty() -> Model {
        return Model (
          query: "",
          passwords: [],
          matches: [],
          selected: 0
        )
    }

    func update(
      query: String? = nil,
      passwords: [Password]? = nil,
      selected: Int? = nil
    ) -> Model {
        let q = query ?? self.query
        let ps = passwords ?? self.passwords
        let ms = ps.filter(match(q))
        return Model(
          query: q,
          passwords: ps,
          matches: ms,
          selected: bounded(selected ?? self.selected, ms.count)
        )
    }

    func reset() -> Model {
        return update(query: "", selected: 0)
    }
}

struct Password {
    let name: String;
    let path: [String];

    func show() -> String {
        switch path {
        case []:
            return name
        case _:
            return "\(path.joined(separator: "/"))/\(name)"
        }
    }
}

private func match(_ q: String) -> (Password) -> Bool {
    if q.isEmpty {
        return { _ in false }
    }

    let parts = q.words()
    return { password in
        let name = password.show()
        return !parts.isEmpty && parts.allSatisfy(
          { name.occur($0).count > 0 }
        )
    }
}

private func bounded(_ n: Int, _ max: Int) -> Int {
    guard max > 0 else {
        return 0
    }
    return (n + max) % max
}

SDK := "macosx"
SWIFTC := xcrun --sdk $(SDK) swiftc

SOURCES=$(wildcard src/*.swift src/**/*.swift)

.PHONY: default
default: dist/FastPass.app

.PHONY: again
again: clean default

.PHONY: clean
clean:
	rm -rf build/ dist/

.PHONY: run
run: dist/FastPass.app
	$^/Contents/MacOS/fastpass

dist/FastPass.app: Info.plist $(wildcard Resources/*) build/fastpass build/FastPass.icns
	mkdir -p $@/Contents/{MacOS,Resources}
	cp Info.plist $@/Contents
	cp build/fastpass $@/Contents/MacOS/
	cp build/FastPass.icns $@/Contents/Resources/

build/fastpass: $(SOURCES) | build
	$(SWIFTC) -warnings-as-errors -emit-executable -o $@ $^

build/FastPass.iconset: Resources/AppIcon.png
	mkdir -p $@
	sips -z 32 32 Resources/AppIcon.png --out $@/icon_32x32.png
	sips -z 64 64 Resources/AppIcon.png --out $@/icon_32x32@2x.png
	sips -z 128 128 Resources/AppIcon.png --out $@/icon_128x128.png
	sips -z 256 256 Resources/AppIcon.png --out $@/icon_128x128@2x.png

build/FastPass.icns: build/FastPass.iconset | build
	iconutil -c icns -o $@ $^

build:
	mkdir $@
